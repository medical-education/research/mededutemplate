
### Ethics approval and consent to participate / Ethikvotum

Participants were asked to complete the test voluntarily and anonymously. To achieve maximum transparency, all participants had to verbally agree to participate. Additionally, they provided their informed consent prior to the study by reading the background information and choosing to provide data. An extra written consent was not obtained. The study and the use of only verbal consent was approved by the Ethics Committee of the Chamber of Physicians at Westfalen-Lippe and Bielefeld University, Medical School OWL (XXXX-YYY-f-S).

Die Teilnehmer wurden gebeten, den Test freiwillig und anonym auszufüllen. Um ein Höchstmaß an Transparenz zu erreichen, mussten alle Teilnehmer ihre mündliche Zustimmung zur Teilnahme geben. Darüber hinaus gaben sie vor der Studie ihre informierte Zustimmung, indem sie die Hintergrundinformationen lasen und sich für die Bereitstellung ihrer Daten entschieden. Eine zusätzliche schriftliche Einwilligung wurde nicht eingeholt. Die Studie und die Verwendung der ausschließlich mündlichen Einwilligung wurde von der Ethikkommission der Ärztekammer Westfalen-Lippe und der Medizinischen Fakultät OWL der Universität Bielefeld genehmigt (XXXX-YYY-f-S).

### Consent for publication / Einwilligung zur Veröffentlichung

Not applicable

Nicht zutreffend

### Availability of data and materials / Verfügbarkeit von Daten und Materialien

The original data that support the findings of this study are available from Open Science Framework (osf.io, see manuscript-URL).

Die Originaldaten der Studie sind beim Open Science Framework (osf.io, siehe Manuskript-URL) verfügbar.

### Competing interests / Konkurrierende Interessen

The authors declare that they have no competing interests.

Die Autoren erklären, dass sie keine konkurrierenden Interessen haben.

### Funding / Finanzierung

The author(s) received no specific funding for this work.

Der/die Autor\*innen erhielt(en) für diese Arbeit keine spezielle Finanzierung.

### Authors' contributions / Beiträge der Autor\*innen

HF conceived the study and participated in its design and coordination. XX participated in the data acquisition and data analysis. YY participated in the study design. ZZ participated in the design and coordination of the study. All authors helped to draft the manuscript.

HF konzipierte die Studie und beteiligte sich an deren Gestaltung und Koordination. XX war an der Datenerfassung und Datenanalyse beteiligt. YY war an der Gestaltung der Studie beteiligt. ZZ beteiligte sich an der Konzeption und Koordination der Studie. Alle Autor\*innen haben an der Erstellung des Manuskripts mitgewirkt.

### CRediT authorship contribution statement

**Hendrik Friederichs:** Conceptualization, Data curation, Formal analysis, Funding acquisition, Investigation, Methodology, Project administration, Resources, Software, Supervision, Validation, Visualization, Writing - review & editing, Writing - original draft. **Wolf Jonas Friederichs:** Conceptualization, Data curation, Formal analysis, Funding acquisition, Investigation, Methodology, Project administration, Resources, Software, Supervision, Validation, Visualization, Writing - review & editing, Writing - original draft. **Maren März:** Conceptualization, Data curation, Formal analysis, Funding acquisition, Investigation, Methodology, Project administration, Resources, Software, Supervision, Validation, Visualization, Writing - review & editing, Writing - original draft.

### Acknowledgments / Danksagung

The authors are grateful for the insightful comments offered by the anonymous peer reviewers at {{< meta citation.container-title >}}. The generosity and expertise of one and all have improved this study in innumerable ways and saved us from many errors; those that inevitably remain are entirely our own responsibility.

Die Autoren sind dankbar für die aufschlussreichen Kommentare der anonymen Peer-Reviewer von {{< meta citation.container-title >}}. Die Großzügigkeit und das Fachwissen eines jeden Einzelnen haben diese Studie auf unzählige Arten verbessert und uns vor vielen Fehlern bewahrt; die, die unvermeidlich bleiben, liegen vollständig in unserer eigenen Verantwortung.
