### Summary / Zusammenfassung der Ergebnisse

After the evaluation of all datasets, the following findings emerged. The first is that ...

Nach Auswertung aller Datensätze ergaben sich die folgenden Erkenntnisse: Die erste ist, dass ...

### Limitation: Studienpopulation

möglicher Einfluss der Studienpopulation auf Interpretation und Anwendbarkeit der Ergebnisse ...

### Limitation: Studiendesign

möglicher Einfluss des Studiendesigns auf Interpretation und Anwendbarkeit der Ergebnisse ...

### Integration with prior work / Vergleich mit bestehender theoretischer und empirischer Forschung

... is a high effect size in comparison to Hattie et al. [@hattie2023visible].

### Implications for practice / Direkte Auswirkungen der Ergebnisse auf Praxis

{{< lipsum 1 >}}

### Implications for research / Direkte Auswirkungen der Ergebnisse auf Forschung

...

## Conclusions / Schlussfolgerungen

...
