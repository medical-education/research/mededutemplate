### Recruitment Process and Demographic Characteristics / Studienteilnahme

The recruitment process is shown in Figure 1. We obtained XX complete data sets (return rate YY.Z%) after contacting ...

Der Rekrutierungsprozess ist in Abbildung 1 dargestellt. Wir erhielten XX vollständige Datensätze (Rücklaufquote YY.Z%), nachdem wir Kontakt mit ...

<!-- Man kann Code-Ergebnisse über {{< embed notebooks/EDA.qmd#fig-map >}} einfügen -->

### Primary and secondary Outcomes / Haupt- und Nebenergebnisse

![Beispielgrafik: ein Bild sagt mehr als tausend Worte ...](Durchschnittswerte_Selbsteinschätzung_NKLM_14a.jpg)

<!-- Man kann Code-Ergebnisse über {{< embed notebooks/EDA.qmd#fig-map >}} einfügen -->
