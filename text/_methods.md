### Setting and subjects / Setting und Probanden

Our study was conducted at Medical School ...

Unsere Studie wurde an der Medizinischen Fakultät der ... durchgeführt.

It takes six years to complete a course in medical school in Germany, with students enrolled directly from secondary schools. The course of study is divided into a pre-clinical section (the first two years) and a clinical section (the last four years). To improve students' clinical experience, they are rotated in various hospital departments during their final year ("clinical/practical" year). ...

Das Medizinstudium in Deutschland dauert sechs Jahre, wobei die Studierenden direkt von den weiterführenden Schulen aufgenommen werden. Das Studium gliedert sich in einen vorklinischen Teil (die ersten beiden Jahre) und einen klinischen Teil (die letzten vier Jahre). Um die klinische Erfahrung der Studenten zu verbessern, werden sie während ihres letzten Jahres (klinisch-praktisches Jahr) in verschiedenen Krankenhausabteilungen eingesetzt.

### Study design / Studiendesign

The participants were asked to complete the BNT voluntarily and anonymously.

### Ethical approval / Ethikvotum

All participants had to agree verbally to participate. Additionally, they provided informed consent prior to the study by reading the background information and choosing to provide data. Ethical approval was given by the Ethics Committee of the Chamber of Physicians at Westfalen-Lippe and Bielefeld University, Medical School OWL (XXXX-YYY-f-S).

Alle Teilnehmer mussten sich mündlich zur Teilnahme bereit erklären. Zusätzlich gaben sie vor der Studie eine informierte Einwilligung, indem sie die Hintergrundinformationen lasen und sich daraufhin für die Bereitstellung ihrer Daten entschieden. Das Ethikvotum wurde von der Ethikkommission der Ärztekammer Westfalen-Lippe und der Universität Bielefeld, Medizinische Fakultät OWL (XXXX-YYY-f-S) erteilt.

### Data collection / Datenerhebung

Data collection for this study was determined à priori as follows:

-   Input ...

Die Datenerhebung für diese Studie wurde à priori wie folgt festgelegt:

-   Input ...

```{webr-r}
#| context: setup

# Download a dataset
download.file(
  'https://raw.githubusercontent.com/coatless/raw-data/main/penguins.csv',
  'penguins.csv'
)

# Read the data
df_penguins = read.csv("penguins.csv")
```

### Outcome Measures / Ergebnisparameter

...

### Statistical methods / Statistische Methoden

We used the standard alpha level of .05 for significance and a power level of .80. Therefore, we needed a sample size of at least XX participants to detect an effect size showing a minimally important difference (d = .YY) [@hattie2023visible] in outcome level between intervention and control groups (calculated *a priori* with G\*Power 3.1) [@faul2007g]. Statistical analysis, tables and figures were conducted using R [@R-base] in RStudio IDE (Posit Software, Boston, MA) with the tidyverse-, gt- and ggstatsplot-packages [@tidyverse; @gt; @patil2021visualizations]. Descriptive means and standard deviations were calculated for participants' age, and total test scores and frequencies were calculated for sex and for solving the case scenarios. Sample means and frequencies were compared with population means and frequencies using one-sample t-tests and chi-square tests, respectively. ...

Wir verwendeten das Standard-Alpha-Niveau von .05 für die Signifikanz und ein Power-Niveau von .80. Daher benötigten wir eine Stichprobengröße von mindestens XX Teilnehmern, um eine Effektgröße nachzuweisen, die einen minimal bedeutsamen Unterschied (d = .YY) [@hattie2023visible] im Ergebnisniveau zwischen Interventions- und Kontrollgruppe zeigt (*a priori* berechnet mit G\*Power 3.1) [@faul2007g]. Statistische Analysen, Tabellen und Abbildungen wurden mit R [@R-base] in RStudio IDE (Posit Software, Boston, MA) mit den tidyverse-, gt- und ggstatsplot-Paketen [@tidyverse; @gt; @patil2021visualizations] durchgeführt. Deskriptive Mittelwerte und Standardabweichungen wurden für das Alter der Teilnehmer berechnet, und die Gesamttestwerte und Häufigkeiten wurden für das Geschlecht und für die Lösung der Fallszenarien berechnet. Die Mittelwerte und Häufigkeiten der Stichprobe wurden mit den Mittelwerten und Häufigkeiten der Grundgesamtheit unter Verwendung von t-Tests bzw. Chi-Quadrat-Tests für eine Stichprobe verglichen. ...

```{webr-r}
#| context: interactive

# Download a dataset
download.file(
  'https://raw.githubusercontent.com/coatless/raw-data/main/penguins.csv',
  'penguins.csv'
) # <1>

# Read the data
penguins = read.csv("penguins.csv") # <2>

# Scatterplot example: penguin bill length versus bill depth
ggplot2::ggplot(data = penguins, ggplot2::aes(x = bill_length_mm, y = bill_depth_mm)) + # <3>
  ggplot2::geom_point(ggplot2::aes(color = species, 
                 shape = species), # <3>
             size = 2)  +
  ggplot2::scale_color_manual(values = c("darkorange","darkorchid","cyan4")) # <3>
```

1.  Download the dataset
2.  Read the data
3.  Build a scatterplot