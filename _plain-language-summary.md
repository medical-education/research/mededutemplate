
plain-language-summary: |
  **Relevantes (Studierenden-)Problem:** 
  <!-- Allgemeineres Problem: mit einem „Helden“ (Studierende oder v.a. junge Ärzte), man selbst als Wegbereiter, Trainer, „Enabler“ --> 
  Die Akzeptanz von dem XYZ-THEMA hat deutlich zugenommen. 
  Für einen Einsatz im professionellen Bereich sind die Leistungen bisher aber allenfalls ausreichend.
  
  **Fokussiertes Problem:** Studienlage zu THEMA allgemein und Medical-Education-Kontext;  
  Progress Tests eignen sich besonders zur Messung von Fortschritt durch Vergleich mit verschiedenen Ausbildungsniveaus.
  
  **Gap des Problems:** 
  <!-- Gap / Dilemma / Widerspruch subj. Erwartung und Realität -->
  Es gibt eine hohe Erwartung an den Einsatz von THEMA in der Medizin. 
  Die bisherigen Leistungen sind auch in der Medizin bisher aber allenfalls ausreichend.  
  
  **Lösung?:** 
  <!-- möglicher Fortschritt / mögliche Lösung - Ansprechen von Motiv(en): Anschluss, Leistung, Macht -->
  Gibt es einen Fortschritt durch bessere Leistungen der neuen Möglichkeiten?  

  **Forschungsfragen:** 
  Wie ist die absolute Leistung von THEMA im Progress Test Medizin?  
  Wie ist die relative Leistung im Vergleich zu Medizinstudierenden?  
  Wie sieht die Leistung bei detaillierter Betrachtung der Domänen und Kompetenzlevel aus?   
  
  **Studienpopulation:** Medizinstudierende.  
  
  **Studiendesign:** Kontrollierte Studie  
  
  **Datenerhebung:**  200 Multiple-Choice-Fragen aus dem Progress Test Medizin   
  
  **Ergebnisparameter:**  Anzahl der richtigen Antworten insgesamt und pro Domäne bzw. Kompetenzlevel
  
  **Statistik:**  Bestimmung der Prozentwerte für die absolute und z-Scores und Percentilen für die relative Bewertung der Leistungen.  




