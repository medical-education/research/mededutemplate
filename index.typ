// Some definitions presupposed by pandoc's typst output.
#let blockquote(body) = [
  #set text( size: 0.92em )
  #block(inset: (left: 1.5em, top: 0.2em, bottom: 0.2em))[#body]
]

#let horizontalrule = [
  #line(start: (25%,0%), end: (75%,0%))
]

#let endnote(num, contents) = [
  #stack(dir: ltr, spacing: 3pt, super[#num], contents)
]

#show terms: it => {
  it.children
    .map(child => [
      #strong[#child.term]
      #block(inset: (left: 1.5em, top: -0.4em))[#child.description]
      ])
    .join()
}



#let article(
  title: none,
  authors: none,
  date: none,
  abstract: none,
  cols: 1,
  margin: (x: 1.25in, y: 1.25in),
  paper: "us-letter",
  lang: "en",
  region: "US",
  font: (),
  fontsize: 11pt,
  sectionnumbering: none,
  toc: false,
  doc,
) = {
  set page(
    paper: paper,
    margin: margin,
    numbering: "1",
  )
  set par(justify: true)
  set text(lang: lang,
           region: region,
           font: font,
           size: fontsize)
  set heading(numbering: sectionnumbering)

  if title != none {
    align(center)[#block(inset: 2em)[
      #text(weight: "bold", size: 1.5em)[#title]
    ]]
  }

  if authors != none {
    let count = authors.len()
    let ncols = calc.min(count, 3)
    grid(
      columns: (1fr,) * ncols,
      row-gutter: 1.5em,
      ..authors.map(author =>
          align(center)[
            #author.name \
            #author.affiliation \
            #author.email
          ]
      )
    )
  }

  if date != none {
    align(center)[#block(inset: 1em)[
      #date
    ]]
  }

  if abstract != none {
    block(inset: 2em)[
    #text(weight: "semibold")[Abstract] #h(1em) #abstract
    ]
  }

  if toc {
    block(above: 0em, below: 2em)[
    #outline(
      title: auto,
      depth: none
    );
    ]
  }

  if cols == 1 {
    doc
  } else {
    columns(cols, doc)
  }
}
#show: doc => article(
  title: [Titel des Manuskripts],
  authors: (
    ( name: [Hendrik Friederichs],
      affiliation: [Universität Bielefeld, Medizinische Fakultät OWL],
      email: [hendrik.friederichs\@uni-bielefeld.de] ),
    ( name: [Lennart Handke],
      affiliation: [Universität Bielefeld, Medizinische Fakultät OWL],
      email: [] ),
    ),
  date: [2023-09-20],
  lang: "de",
  abstract: [#strong[Background / Hintergrund]: …

#strong[Methods / Methoden]: …

#strong[Results / Ergebnisse]: …

#strong[Conclusio / Schlussfolgerungen]: …

],
  cols: 1,
  doc,
)


#super[\*] These authors contributed equally to this work.

#super[1] Universität Bielefeld, Medizinische Fakultät OWL

#super[✉] Correspondence: #link("mailto:hendrik.friederichs@uni-bielefeld.de")[Hendrik Friederichs \<hendrik.friederichs\@uni-bielefeld.de\>]

#blockquote[
#strong[IN PROGRESS …]

This manuscript is a work in progress. However, thank you for your interest. Please feel free to visit this web site again at a later date.

#emph[Dieses Manuskript ist noch in Arbeit. Wir danken Ihnen jedoch für Ihr Interesse. Bitte besuchen Sie diese Website zu einem späteren Zeitpunkt noch einmal …]
]

#blockquote[
#strong[STRUKTUR DES MANUSKRIPTS]

#strong[Relevantes (Studierenden-)Problem:] Die Akzeptanz von THEMA hat deutlich zugenommen. Für einen Einsatz im professionellen Bereich sind die Leistungen bisher aber allenfalls ausreichend. \
#strong[Fokussiertes Problem:] Studienlage zu THEMA allgemein und Medical-Education-Kontext; \
Progress Tests eignen sich besonders zur Messung von Fortschritt durch Vergleich mit verschiedenen Ausbildungsniveaus. \
#strong[Gap des Problems:] Es gibt eine hohe Erwartung an den Einsatz von THEMA in der Medizin. Die bisherigen Leistungen sind auch in der Medizin bisher aber allenfalls ausreichend. \
#strong[Lösung?:] Gibt es einen Fortschritt durch bessere Leistungen der neuen Möglichkeiten? \
#strong[Forschungsfragen:] Wie ist die absolute Leistung von THEMA im Progress Test Medizin? \
Wie ist die relative Leistung im Vergleich zu Medizinstudierenden? \
Wie sieht die Leistung bei detaillierter Betrachtung der Domänen und Kompetenzlevel aus? \
#strong[Studienpopulation:] Medizinstudierende. \
#strong[Studiendesign:] Kontrollierte Studie \
#strong[Datenerhebung:] 200 Multiple-Choice-Fragen aus dem Progress Test Medizin \
#strong[Ergebnisparameter:] Anzahl der richtigen Antworten insgesamt und pro Domäne bzw. Kompetenzlevel \
#strong[Statistik:] Bestimmung der Prozentwerte für die absolute und z-Scores und Percentilen für die relative Bewertung der Leistungen.
]

== Background / Hintergrund
<background-hintergrund>
=== Broad problem / Allgemeineres Problem
<broad-problem-allgemeineres-problem>
…

=== Theoretical and/or empirical focus of the problem / Theoretische und/oder empirische Fokussierung des Problems
<theoretical-andor-empirical-focus-of-the-problem-theoretische-undoder-empirische-fokussierung-des-problems>
…

=== Focused problem statement / Fokussiertes Problem-Statement: Gap und möglicher Fortschritt
<focused-problem-statement-fokussiertes-problem-statement-gap-und-möglicher-fortschritt>
…

=== Statement of study intent / Fokussierte Forschungsfrage/n
<statement-of-study-intent-fokussierte-forschungsfragen>
We performed a study of medical students to investigate the following questions:

+ What is …
+ Why are …

Wir haben eine Studie mit Medizinstudierenden durchgeführt, um die folgenden Fragen zu untersuchen:

+ Was ist …
+ Warum sind …

== Methods / Methoden
<methods-methoden>
=== Setting and subjects / Setting und Probanden
<setting-and-subjects-setting-und-probanden>
Our study was conducted at Medical School …

Unsere Studie wurde an der Medizinischen Fakultät der … durchgeführt.

It takes six years to complete a course in medical school in Germany, with students enrolled directly from secondary schools. The course of study is divided into a pre-clinical section (the first two years) and a clinical section (the last four years). To improve students’ clinical experience, they are rotated in various hospital departments during their final year ("clinical/practical" year). …

Das Medizinstudium in Deutschland dauert sechs Jahre, wobei die Studierenden direkt von den weiterführenden Schulen aufgenommen werden. Das Studium gliedert sich in einen vorklinischen Teil (die ersten beiden Jahre) und einen klinischen Teil (die letzten vier Jahre). Um die klinische Erfahrung der Studenten zu verbessern, werden sie während ihres letzten Jahres (klinisch-praktisches Jahr) in verschiedenen Krankenhausabteilungen eingesetzt.

=== Study design / Studiendesign
<study-design-studiendesign>
The participants were asked to complete the BNT voluntarily and anonymously.

=== Ethical approval / Ethikvotum
<ethical-approval-ethikvotum>
All participants had to agree verbally to participate. Additionally, they provided informed consent prior to the study by reading the background information and choosing to provide data. Ethical approval was given by the Ethics Committee of the Chamber of Physicians at Westfalen-Lippe and Bielefeld University, Medical School OWL (XXXX-YYY-f-S).

Alle Teilnehmer mussten sich mündlich zur Teilnahme bereit erklären. Zusätzlich gaben sie vor der Studie eine informierte Einwilligung, indem sie die Hintergrundinformationen lasen und sich daraufhin für die Bereitstellung ihrer Daten entschieden. Das Ethikvotum wurde von der Ethikkommission der Ärztekammer Westfalen-Lippe und der Universität Bielefeld, Medizinische Fakultät OWL (XXXX-YYY-f-S) erteilt.

=== Data collection / Datenerhebung
<data-collection-datenerhebung>
Data collection for this study was determined à priori as follows:

- Input …

Die Datenerhebung für diese Studie wurde à priori wie folgt festgelegt:

- Input …

```{webr-r}
#| context: setup

# Download a dataset
download.file(
  'https://raw.githubusercontent.com/coatless/raw-data/main/penguins.csv',
  'penguins.csv'
)

# Read the data
df_penguins = read.csv("penguins.csv")
```

=== Outcome Measures / Ergebnisparameter
<outcome-measures-ergebnisparameter>
…

=== Statistical methods / Statistische Methoden
<statistical-methods-statistische-methoden>
We used the standard alpha level of .05 for significance and a power level of .80. Therefore, we needed a sample size of at least XX participants to detect an effect size showing a minimally important difference (d \= .YY) \[1\] in outcome level between intervention and control groups (calculated #emph[a priori] with G\*Power 3.1) \[2\]. Statistical analysis, tables and figures were conducted using R \[3\] in RStudio IDE (Posit Software, Boston, MA) with the tidyverse-, gt- and ggstatsplot-packages \[4–6\]. Descriptive means and standard deviations were calculated for participants’ age, and total test scores and frequencies were calculated for sex and for solving the case scenarios. Sample means and frequencies were compared with population means and frequencies using one-sample t-tests and chi-square tests, respectively. …

Wir verwendeten das Standard-Alpha-Niveau von .05 für die Signifikanz und ein Power-Niveau von .80. Daher benötigten wir eine Stichprobengröße von mindestens XX Teilnehmern, um eine Effektgröße nachzuweisen, die einen minimal bedeutsamen Unterschied (d \= .YY) \[1\] im Ergebnisniveau zwischen Interventions- und Kontrollgruppe zeigt (#emph[a priori] berechnet mit G\*Power 3.1) \[2\]. Statistische Analysen, Tabellen und Abbildungen wurden mit R \[3\] in RStudio IDE (Posit Software, Boston, MA) mit den tidyverse-, gt- und ggstatsplot-Paketen \[4–6\] durchgeführt. Deskriptive Mittelwerte und Standardabweichungen wurden für das Alter der Teilnehmer berechnet, und die Gesamttestwerte und Häufigkeiten wurden für das Geschlecht und für die Lösung der Fallszenarien berechnet. Die Mittelwerte und Häufigkeiten der Stichprobe wurden mit den Mittelwerten und Häufigkeiten der Grundgesamtheit unter Verwendung von t-Tests bzw. Chi-Quadrat-Tests für eine Stichprobe verglichen. …

```{webr-r}
#| context: interactive

# Download a dataset
download.file(
  'https://raw.githubusercontent.com/coatless/raw-data/main/penguins.csv',
  'penguins.csv'
) # <1>

# Read the data
penguins = read.csv("penguins.csv") # <2>

# Scatterplot example: penguin bill length versus bill depth
ggplot2::ggplot(data = penguins, ggplot2::aes(x = bill_length_mm, y = bill_depth_mm)) + # <3>
  ggplot2::geom_point(ggplot2::aes(color = species, 
                 shape = species), # <3>
             size = 2)  +
  ggplot2::scale_color_manual(values = c("darkorange","darkorchid","cyan4")) # <3>
```

+ Download the dataset
+ Read the data
+ Build a scatterplot

== Results / Ergebnisse
<results-ergebnisse>
=== Recruitment Process and Demographic Characteristics / Studienteilnahme
<recruitment-process-and-demographic-characteristics-studienteilnahme>
The recruitment process is shown in Figure 1. We obtained XX complete data sets (return rate YY.Z%) after contacting …

Der Rekrutierungsprozess ist in Abbildung 1 dargestellt. Wir erhielten XX vollständige Datensätze (Rücklaufquote YY.Z%), nachdem wir Kontakt mit …

=== Primary and secondary Outcomes / Haupt- und Nebenergebnisse
<primary-and-secondary-outcomes-haupt--und-nebenergebnisse>
…

== Discussion / Diskussion
<discussion-diskussion>
=== Summary / Zusammenfassung der Ergebnisse
<summary-zusammenfassung-der-ergebnisse>
After the evaluation of all datasets, the following findings emerged. The first is that …

Nach Auswertung aller Datensätze ergaben sich die folgenden Erkenntnisse: Die erste ist, dass …

=== Limitation: Studienpopulation
<limitation-studienpopulation>
möglicher Einfluss der Studienpopulation auf Interpretation und Anwendbarkeit der Ergebnisse …

=== Limitation: Studiendesign
<limitation-studiendesign>
möglicher Einfluss des Studiendesigns auf Interpretation und Anwendbarkeit der Ergebnisse …

=== Integration with prior work / Vergleich mit bestehender theoretischer und empirischer Forschung
<integration-with-prior-work-vergleich-mit-bestehender-theoretischer-und-empirischer-forschung>
… is a high effect size in comparison to Hattie et al. \[1\].

=== Implications for practice / Direkte Auswirkungen der Ergebnisse auf Praxis
<implications-for-practice-direkte-auswirkungen-der-ergebnisse-auf-praxis>
…

=== Implications for research / Direkte Auswirkungen der Ergebnisse auf Forschung
<implications-for-research-direkte-auswirkungen-der-ergebnisse-auf-forschung>
…

== Conclusions / Schlussfolgerungen
<conclusions-schlussfolgerungen>
…

== References
<references>
<refs>
<ref-hattie2023visible>
1. Hattie J. Visible learning: The sequel: A synthesis of over 2,100 meta-analyses relating to achievement. Taylor & Francis; 2023.

<ref-faul2007g>
2. Faul F, Erdfelder E, Lang A-G, Buchner A. G\* Power 3: A flexible statistical power analysis program for the social, behavioral, and biomedical sciences. Behavior research methods. 2007;39:175–91.

<ref-R-base>
3. R Core Team. #link("https://www.R-project.org")[R: A Language and Environment for Statistical Computing]. Vienna, Austria: R Foundation for Statistical Computing; 2019.

<ref-tidyverse>
4. Wickham H, Averick M, Bryan J, Chang W, McGowan LD, François R, u. a. #link("https://doi.org/10.21105/joss.01686")[Welcome to the tidyverse]. 2019;4:1686.

<ref-gt>
5. Iannone R, Cheng J, Schloerke B, Hughes E, Seo J. #link("https://CRAN.R-project.org/package=gt")[gt: Easily Create Presentation-Ready Display Tables]. 2022.

<ref-patil2021visualizations>
6. Patil I. Visualizations with statistical details: The’ggstatsplot’approach. Journal of Open Source Software. 2021;6:3167.

== Declarations
<declarations>
=== Ethics approval and consent to participate / Ethikvotum
<ethics-approval-and-consent-to-participate-ethikvotum>
Participants were asked to complete the test voluntarily and anonymously. To achieve maximum transparency, all participants had to verbally agree to participate. Additionally, they provided their informed consent prior to the study by reading the background information and choosing to provide data. An extra written consent was not obtained. The study and the use of only verbal consent was approved by the Ethics Committee of the Chamber of Physicians at Westfalen-Lippe and Bielefeld University, Medical School OWL (XXXX-YYY-f-S).

Die Teilnehmer wurden gebeten, den Test freiwillig und anonym auszufüllen. Um ein Höchstmaß an Transparenz zu erreichen, mussten alle Teilnehmer ihre mündliche Zustimmung zur Teilnahme geben. Darüber hinaus gaben sie vor der Studie ihre informierte Zustimmung, indem sie die Hintergrundinformationen lasen und sich für die Bereitstellung ihrer Daten entschieden. Eine zusätzliche schriftliche Einwilligung wurde nicht eingeholt. Die Studie und die Verwendung der ausschließlich mündlichen Einwilligung wurde von der Ethikkommission der Ärztekammer Westfalen-Lippe und der Medizinischen Fakultät OWL der Universität Bielefeld genehmigt (XXXX-YYY-f-S).

=== Consent for publication / Einwilligung zur Veröffentlichung
<consent-for-publication-einwilligung-zur-veröffentlichung>
Not applicable

Nicht zutreffend

=== Availability of data and materials / Verfügbarkeit von Daten und Materialien
<availability-of-data-and-materials-verfügbarkeit-von-daten-und-materialien>
The original data that support the findings of this study are available from Open Science Framework (osf.io, see manuscript-URL).

Die Originaldaten der Studie sind beim Open Science Framework (osf.io, siehe Manuskript-URL) verfügbar.

=== Competing interests / Konkurrierende Interessen
<competing-interests-konkurrierende-interessen>
The authors declare that they have no competing interests.

Die Autoren erklären, dass sie keine konkurrierenden Interessen haben.

=== Funding / Finanzierung
<funding-finanzierung>
The author(s) received no specific funding for this work.

Der/die Autor\*innen erhielt(en) für diese Arbeit keine spezielle Finanzierung.

=== Authors’ contributions / Beiträge der Autor\*innen
<authors-contributions-beiträge-der-autorinnen>
HF conceived the study and participated in its design and coordination. XX participated in the data acquisition and data analysis. YY participated in the study design. ZZ participated in the design and coordination of the study. All authors helped to draft the manuscript.

HF konzipierte die Studie und beteiligte sich an deren Gestaltung und Koordination. XX war an der Datenerfassung und Datenanalyse beteiligt. YY war an der Gestaltung der Studie beteiligt. ZZ beteiligte sich an der Konzeption und Koordination der Studie. Alle Autor\*innen haben an der Erstellung des Manuskripts mitgewirkt.

=== CRediT authorship contribution statement
<credit-authorship-contribution-statement>
#strong[Hendrik Friederichs:] Conceptualization, Data curation, Formal analysis, Funding acquisition, Investigation, Methodology, Project administration, Resources, Software, Supervision, Validation, Visualization, Writing - review & editing, Writing - original draft. #strong[Wolf Jonas Friederichs:] Conceptualization, Data curation, Formal analysis, Funding acquisition, Investigation, Methodology, Project administration, Resources, Software, Supervision, Validation, Visualization, Writing - review & editing, Writing - original draft. #strong[Maren März:] Conceptualization, Data curation, Formal analysis, Funding acquisition, Investigation, Methodology, Project administration, Resources, Software, Supervision, Validation, Visualization, Writing - review & editing, Writing - original draft.

=== Acknowledgments / Danksagung
<acknowledgments-danksagung>
The authors are grateful for the insightful comments offered by the anonymous peer reviewers at Medical Education Online. The generosity and expertise of one and all have improved this study in innumerable ways and saved us from many errors; those that inevitably remain are entirely our own responsibility.

Die Autoren sind dankbar für die aufschlussreichen Kommentare der anonymen Peer-Reviewer von Medical Education Online. Die Großzügigkeit und das Fachwissen eines jeden Einzelnen haben diese Studie auf unzählige Arten verbessert und uns vor vielen Fehlern bewahrt; die, die unvermeidlich bleiben, liegen vollständig in unserer eigenen Verantwortung.
